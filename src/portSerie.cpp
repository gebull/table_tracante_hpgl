/* portSerie.cpp   4/04/19 bug compil. conditionnelle corrigée 11/05/19
         Initialisation du COM1 ou adaptateur usb<->RS232
     L'utilisateur doit faire partie du groupe dialout.
     Relier le broches RTS et CTS (côté RS232).
     Dans le cas de l'adaptateur usb, celui-ci doit être présent lors du lancement du logiciel. 
*/
#include <gpl.h>
#include <def_gen.h>
#include <prmCom.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

#ifdef USB
 #define COM1 "/dev/ttyUSB0"
#else
 #define COM1 "/dev/ttyS0"
#endif

#define POSIX_SOURCE 1 /* POSIX compliant source */

#define TTREF 7 //nombre de vitesses traitées

static speed_t tabVit[TTREF] = {B150, B300, B600,B1200, B2400, B4800, B9600};
static const char * tabRef[TTREF] = {"150", "300", "600", "1200", "2400", "4800", "9600"};

extern int portId; //doit être global

int initCom1(int indexVit)
{
unsigned ctr;
int retRead;
char tmpIni[100];//utilisé uniquement pour l'initialisation
struct termios config;
portId = open(COM1, O_RDWR | O_NOCTTY );
if (portId <0)
 {
 perror(COM1);
 return (-1);
 }
memset(&config,0,sizeof(config));
config.c_cflag = tabVit[indexVit] | CRTSCTS | CS8 | CLOCAL | CREAD;
config.c_iflag = IGNPAR;//ne tient pas compte des erreurs de parité -> ATTENTION
config.c_oflag = 0; //pas de restriction sur l'émission
config.c_lflag = 0; //non canonique, pas d'écho, ...
fcntl(portId, F_SETFL, FNDELAY); //mode non bloquant
tcflush(portId, TCIFLUSH);
tcsetattr(portId,TCSANOW,&config);
ctr=0; retRead =0;
do
 {
 read(portId, tmpIni, 100);//pour vider une éventuelle réception
 ctr++;
 }
 while ((retRead !=0) && (ctr <50));
if (ctr >= 50)
  return (-2);
cout<<"vitesse port série = "<<tabRef[indexVit]<<" bauds\n";
return 0;
}

int verifVitesse (char * entVit)
{
int i;
for (i=0; i< TTREF; i++)
 {
 if ( (strcmp(entVit, tabRef[i]))==0)// comparaison avec table 
   break;
 }
if (i < TTREF)
  return (i);
 else
  return (-1);
}
