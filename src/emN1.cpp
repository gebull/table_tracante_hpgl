/* emN1.cpp   7/04/19  %11/04/19  4/05/19
   émission niveau 1 */

#include <emN1.h>
#include <seq.h>
#include <assert.h>
#include <iostream>
#include <syst1.h>
//---- #include <sys/time.h> pas pour C++ ! c'est unistd.h qu'il faut
#include <unistd.h>

extern Seq3Niv sequenceur;
extern const unsigned NBMAXT;
extern int tabSig[];
extern char tmpUtil[];
extern int nbCarEm; //nombre de caractères à émettre
extern char tmpEm[];
extern int ctrInit;
extern int maxFen;
extern int tempo;
extern int emLib;

#define INITEMN1 DEBUT
#define ATSIG (INITEMN1+1) //attente signal de la part de gestN2
#define OUVFIC (ATSIG +1) //ouverture fichier
#define FINEMIS (OUVFIC +1)
#define CTFEN (FINEMIS +1)

Octet EmN1 :: tache()
{
unsigned div;
int intdiv;  
switch(memEtape)
 {
 case INITEMN1 :
  ctrInit++;
  #ifdef TRACE1
   std::cout << "emN1 : OK, idT= "<< (int)indicat()<<"  --info1= "<< getInfo1() <<"\n";
   std::cout<<"ctrInit = "<<ctrInit<<" \n";
  #endif
  nbCarEm =0;//tampon émission vide
  emLib = 1;
  ptrEmN0 = new EmN0();
  assert(ptrEmN0 != NULL);
  div = sequenceur.niv0.monTache(ptrEmN0);
  if (div >= NBMAXT)
    affErr(div, NIV0);
  memEtape = ATSIG;
  break;
 case ATSIG :
  if (tabSig[idT] == DEMIS)
    {
    #ifdef TRACE1
     cout<<"tmpUtil: "<<tmpUtil<<"\n";
    #endif
    fluxFic.open (tmpUtil , ios::in |ios::binary);// si caractères numériques qq
    // -----fluxFic.open (tmpUtil , ios::in); //si fichier texte (ASCII)
    if (fluxFic) //l'ouverture est réussie
      {
      cout<<" ouverture fichier OK \n";
      ctrInstr=0;
      nCarFic=0;
      ctrBloc=0;
      ctrFen=0;
      ctrLn=0;
      ctrDrtiv=0;
      numBloc=0;
      pireCas=0;
      ctPrec=0;
      cmptInstr();
      if (ctrInstr == 0)
        {
        cout<<"un fichier doit comporter au moins une instruction ! \n";
        tabSig[idT] = NOSIG;
        fluxFic.close();
        memEtape = ATSIG;
        }
       else
        {
        nBlocs = nCarFic / TBLOC;
        restCar = nCarFic % TBLOC;
        cout<<"le fichier comporte: "<<dec<<nCarFic<<" caractères \n";
        cout<<"le fichier comporte: "<<dec<<ctrInstr<<" instructions \n";
        cout<<"pire cas: "<<dec<<pireCas<<" caractères/instruction \n";
        cout<<"le fichier comporte: "<<dec<<ctrDrtiv<<" directives \n";
        cout<<"le fichier comporte: "<<dec<<ctrLn<<" lignes \n";
        cout<<"le fichier comporte: "<<dec<<nBlocs<<" blocs complets \n";
        // --- fluxFic.open (tmpUtil , ios::in);
        fluxFic.open (tmpUtil , ios::in |ios::binary);
        fluxFic.seekg(0, ios::beg); //replace en début de fichier
        memEtape = OUVFIC;
        }
      }
     else
      {
      cout<<"l'ouverture du fichier a échoué !\n";
      tabSig[idT] = NOSIG;
      memEtape = ATSIG;
      }
    }
   else
    memEtape = ATSIG;
  break;
 case OUVFIC :
  if (emLib  == 1) // emN0 libre  
    {
    intdiv = calCulDebut();
    #ifdef TRACE2
    cout<<"début= "<< intdiv<< " \n";
    #endif
    if (intdiv <0)
      {
      if (!restCar)
        memEtape = FINEMIS;
       else
        {
        intdiv = ctrBloc * TBLOC;
        intdiv = copTmpEm(intdiv, restCar);
        restCar =0;
        nbCarEm = intdiv;
        memEtape = OUVFIC;
        }
      }
     else //émission de blocs
      {
      intdiv = copTmpEm(intdiv, TBLOC);
      ctrBloc++;
      nbCarEm = intdiv;
      numBloc++;
      cout<<"numéro de bloc: "<<dec<<numBloc<<"\n";
      memEtape = CTFEN;
      }  
    }
   else
    {
    if (emLib == -1)
      {
      cout<<"ABORT emN1";
      tabSig[idT] = ABORT;
      cout<<" ABORT emN1 ";
      memEtape = FIN;
      }
     else //non libre sans erreur (en principe 0)
      memEtape = OUVFIC; //attente emN0 libre 
    }
  break;
 case FINEMIS:
  tabSig[idT] = NOSIG;
  fluxFic.close();
  ctrInstr =0;
  cout<<"fin émission \n";
   tabSig[idT] = ABORT; //arrêt niveau 2 -> fin programme
   memEtape = FIN;
  break;
 case CTFEN:
  memEtape = OUVFIC;
  ctrFen++;
  if (ctrFen >= maxFen)
    {
    ctrFen =0;
    #ifdef TRACE2
    cout<<"tempo ici !\n";
    #endif
    usleep(1000 * tempo); //-> tempo en secondes
    }
  break;
 default :
  assert(0);
 }
return finTache();
}

int EmN1::calCulDebut(void)
 {
 if (ctrBloc < nBlocs)
   return ctrBloc * TBLOC;
  else
   return -1;
 }

int EmN1::copTmpEm(int debut ,unsigned nbC)
 {
 unsigned i=0;
 Octet carLu;
 assert(nbC < TTMPEM);
 fluxFic.seekg(debut, ios::beg); //replace au début pour bloc (entier ou non)
 while (i<nbC)
  {
  carLu= fluxFic.get();
  tmpEm[i] = carLu;
  i++;
  }
 return i;
 }


void EmN1::cmptInstr(void)
 {
 unsigned delta;
 fluxFic.seekg(0, ios::beg); //replace en début de fichier
 int carLu = 'a'; //n'importe quoi différent de EOF
 while(carLu != EOF)
  {
  nCarFic++;
  carLu= fluxFic.get();
  if (carLu == ';')
    {
    ctrInstr++;
    delta = nCarFic - ctPrec;
    ctPrec = nCarFic;
    if (delta > pireCas)
      {
      pireCas = delta;
      }
    }
  if (carLu == '\n')
    ctrLn++;
  if (carLu == ',')
    ctrDrtiv++;
  }
 fluxFic.close();//il faut fermer pour pouvoir le ré-ouvrir ?!?
 }               //il semble que la détection de EOF ferme le fichier ?!?

