/*   syst1.h   4/04/19
*/
#ifndef SYST1_
#define SYST1_

// liste des erreurs : messages dans syst1.cpp
#define ERREUR 1000 //erreur générique
#define IDENTICOMP 999 //identif demandé incompatible (trop grande ....)
#define NONMONTE   998 //indentificateur ne correspond à aucune tâche montée
#define DEJACTIV   997 //ne peut être activée car déjà activée
#define NIVEAUPLEIN 996 //plus de place dans le niveau
#define DEJACT      995 //déjà activé
#define DEJAUTILISE 994 //identif est déjà prise par une autre fonction
#define EXECACTIV   993 //en cours d'exécution, ne peut être démontée

/*
#define NIVEAUPLEIN 999 //plus de place dans le niveau
#define ERRACTIV    997 //erreur d'activation
#define NONLIB      996 //plus de trame libre dans la banque 
#define NONREND     995 //cette trame ne peut être rendue
#define IMPOSPLUS   994 //refus de monter une tâche (plus d'une fois)
#define NIVNOK      993 //le niveau demandé est incorrect
#define ERRMONT     992 //erreur de montage
*/
#define PLUSPTE     992 //plus petite erreur,à mettre à jour si nouvelles
                        // erreurs introduites

/*---------------------*/
#define NERREUR  0 // pas d'erreur
#define SANS 10000 // pas d'info complémentaire à afficher
/*----------------------------------------------------------*/

void affErr(unsigned noErr, int infCompl= SANS ) ;

/*----------------------------------------------------------*/
#endif //SYST1_
