/* def_gen.h    4/04/19
       macros, types et paramètres généraux
              *********************************** */

#include <gpl.h>

#ifndef DEF_GEN_
#define DEF_GEN_

using namespace std;//pour iostream et fstream, évite std::cout etc...

#define VRAI 1
#define FAUX 0

#define NOSIG 0 //pas de signal entre tâches (en général) ici emN1 fin émission
#define DEMIS 100 //gestN2 indique à emN1 que émission d'un fichier est requise
#define ABORT 101 //fin sur émission saturée

typedef int Booleen;

typedef unsigned char Octet; //8bits
typedef unsigned short int Mot; //mot de 16 bits
typedef unsigned long int Dmot; //double mot = 32 bits
#define TTMPREC 150 //taille d'un tampon réception (tmpRec_x)
typedef Octet TmpRec[TTMPREC] ;

#define AVERTF 30 //avertisement pour une fenêtre "trop grande"
#define NCNOM 130 //nombre de caractères maxi. pour le nom d'un fichier
// --------------------------------------
#ifdef USB
 #define TBLOC 600 //cas port usb, taille d'un bloc de caractères
#else
 #define TBLOC 57 //cas port série direct taille d'un bloc de caractères
#endif
// --------------------------------------
#define TTMPEM (TBLOC +2)

#define NBINIT   5 // tâches: gestN2, emN1, emN0, recN1, recN0

#define TTABREC 10  //taille de la table (tabEtat) de structures (etatRec)

#define CARFIN1 '\n'
#define CARFIN2 '\0'

#define TTRAIT 500 // taille du tampon de traitement

//#define PASN2 100000 //limite les passages au N2 (ctrPasN2)
#define PASN2 2 //limite les passages au N2 (ctrPasN2)

#endif /*DEF_GEN_*/
