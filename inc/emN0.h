/* emN0.h   9/04/19
    émission au niveau 0 du séquenceur */

#include <def_gen.h>
#include <tache.h>

#ifndef EMN0_
#define EMN0_

class EmN0 : public TacheGen
{
 Octet tache ();
public:
 EmN0( int info1= NOINFO, unsigned etape = DEBUT) : TacheGen(info1, etape) { }
};
#endif // EMN0_

