/*  emN1.h  7/04/19  %11/04/19
   émission niveau 1 */

using namespace std;//pour iostream et fstream
#include <tache.h>
#include <fstream>
#include <emN0.h>

#ifndef EMN1_
#define EMN1_

class EmN1 : public TacheGen
{
 Octet tache ();
 EmN0 * ptrEmN0;
 ifstream fluxFic ;
 unsigned ctrInstr; //compte les instructions (séparées par des ';')
 unsigned nCarFic; //nombre de caractères dans le fichier 
 unsigned nBlocs; //nombre de caractères dans un bloc complet
 unsigned restCar; //nomde caractères restants (bloc incomplet)
 unsigned ctrBloc; //numérotation des blocs (commence à 0)
 unsigned ctrLn; // compteur de lignes du fichier
 unsigned ctrDrtiv; //compteur de directives (ie le nombre de virgules ',')
 unsigned numBloc; //mumérotation des blocs (commence à1)
 unsigned pireCas; //instruction la plus longue
 unsigned ctPrec; //compteur de caractères pour l'instruction précédente
 int ctrFen; // compteur pour fenêtre -> tempo.
 int calCulDebut(void); //indique la position dans le fichier pour bloc suivant
 int copTmpEm(int debut, unsigned nbC); //copie des caractères dans le tampon émission
 void cmptInstr(void); //compte le instructions (ie les ;) et autres...
public:
 EmN1( int info1= NOINFO, unsigned etape = DEBUT) : TacheGen(info1, etape) { }
};

#endif // EMN1
